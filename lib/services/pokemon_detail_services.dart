import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:pokemon_app/datasource/pokemon_detail_datasource.dart';
import 'package:pokemon_app/models/pokemon_detail_model.dart';
import 'package:pokemon_app/utils/api_status.dart';
import 'package:pokemon_app/utils/constants.dart';

class PokemonDetailServices implements PokemonDetailDatasource {
  final Dio dio;
  PokemonDetailServices({required this.dio});
  @override
  Future<Object> getApiDetail(int index) async {
    try {
      String pokemonDetailPathApi = '$pokemonDetailPath$index';
      final response = await dio.get(pokemonDetailPathApi);
      PokemonDetailModels pokemonDetailModels = PokemonDetailModels.fromJson(response.data);
      return Success(response: pokemonDetailModels);
    } on DioException catch (e) {
      return Failure(code: e.response?.statusCode, errorResponse: e.response?.statusMessage ?? '');
    }
  }
}
