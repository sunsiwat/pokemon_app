import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:pokemon_app/datasource/pokemon_datasource.dart';
import 'package:pokemon_app/models/pokemon_list_model.dart';
import 'package:pokemon_app/utils/api_status.dart';
import 'package:pokemon_app/utils/constants.dart';

class PokemonServices implements PokemonDatasource {
  final Dio dio;
  PokemonServices({required this.dio});
  @override
  Future<Object> getApiPokemon(int offsetApi) async {
    try {
      String pokemonPathApi = '$pokemonPath$offsetApi';
      log(pokemonPathApi);
      final response = await dio.get(pokemonPathApi);
      PokemonListModels pokemonList = PokemonListModels.fromJson(response.data['results']);
      return Success(response: pokemonList);
    } on DioException catch (e) {
      return Failure(code: e.response?.statusCode, errorResponse: e.response?.statusMessage ?? '');
    }
  }
}
