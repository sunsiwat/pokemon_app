class PokemonListModels {
  List<Pokemon> pokemonList;

  PokemonListModels({
    required this.pokemonList,
  });

  factory PokemonListModels.fromJson(List<dynamic> listJson) {
    List<Pokemon> pokemonList = listJson.map<Pokemon>((pokemonJson) => Pokemon.fromJson(pokemonJson)).toList();

    return PokemonListModels(
      pokemonList: pokemonList,
    );
  }
}

class Pokemon {
  final String name;
  final String url;

  Pokemon({
    required this.name,
    required this.url,
  });

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    return Pokemon(
      name: json['name'],
      url: json['url'],
    );
  }
}
