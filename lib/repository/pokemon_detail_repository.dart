import 'package:pokemon_app/datasource/pokemon_detail_datasource.dart';
import 'package:pokemon_app/services/pokemon_detail_services.dart';

class PokemonDetailRepository implements PokemonDetailDatasource {
  final PokemonDetailServices pokemonDetailServices;
  PokemonDetailRepository({required this.pokemonDetailServices});

  @override
  Future<Object> getApiDetail(int index) {
    return pokemonDetailServices.getApiDetail(index);
  }
}
