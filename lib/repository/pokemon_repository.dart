import 'package:pokemon_app/datasource/pokemon_datasource.dart';
import 'package:pokemon_app/services/pokemon_services.dart';

class PokemonRepository implements PokemonDatasource {
  final PokemonServices pokemonServices;
  PokemonRepository({required this.pokemonServices});

  @override
  Future<Object> getApiPokemon(int offsetApi) {
    return pokemonServices.getApiPokemon(offsetApi);
  }
}
