import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon_app/repository/pokemon_detail_repository.dart';
import 'package:pokemon_app/repository/pokemon_repository.dart';
import 'package:pokemon_app/screen/views/home_page.dart';
import 'package:pokemon_app/services/pokemon_detail_services.dart';
import 'package:pokemon_app/services/pokemon_services.dart';
import 'package:pokemon_app/view_models/pokemon_view_models.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => PokemonViewModels(
              pokemonRepository: PokemonRepository(pokemonServices: PokemonServices(dio: Dio())),
              pokemonDetailRepository:
                  PokemonDetailRepository(pokemonDetailServices: PokemonDetailServices(dio: Dio()))),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(360, 640),
        child: MaterialApp(
          title: 'Pokemon Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: const HomePage(),
        ),
      ),
    );
  }
}
