import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:pokemon_app/models/pokemon_detail_model.dart';
import 'package:pokemon_app/models/pokemon_list_model.dart';
import 'package:pokemon_app/repository/pokemon_detail_repository.dart';
import 'package:pokemon_app/repository/pokemon_repository.dart';
import 'package:pokemon_app/utils/api_status.dart';
import 'package:pokemon_app/utils/constants.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

enum HomeState {
  initial,
  loading,
  success,
  error,
}

class PokemonViewModels extends ChangeNotifier {
  final PokemonRepository pokemonRepository;
  final PokemonDetailRepository pokemonDetailRepository;
  PokemonViewModels({required this.pokemonRepository, required this.pokemonDetailRepository});

  int _index = 0;
  int _offsetApi = 0;
  String _errorMessage = '';
  String _filter = '';
  HomeState _appState = HomeState.initial;
  PokemonDetailModels? _pokemonDetailModels;
  PokemonListModels _pokemonListModels = PokemonListModels(pokemonList: []);
  PokemonListModels _pokemonListModelsFilter = PokemonListModels(pokemonList: []);
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  int get offsetApi => _offsetApi;
  String get errorMessage => _errorMessage;
  String get filter => _filter;
  HomeState get appState => _appState;
  PokemonDetailModels? get pokemonDetailModels => _pokemonDetailModels;
  PokemonListModels get pokemonListModels => _pokemonListModels;
  PokemonListModels get pokemonListModelsFilter => _pokemonListModelsFilter;
  RefreshController get refreshController => _refreshController;

  void onRefresh() async {
    _pokemonListModels.pokemonList = [];
    _offsetApi = 0;
    await getData();
    _refreshController.refreshCompleted();
  }

  void onLoading() async {
    _offsetApi += limitApi;
    await getData();
    _refreshController.loadComplete();
  }

  void setErrorMessage(String error) {
    _errorMessage = error;
    _appState = HomeState.error;
    notifyListeners();
  }

  void setPokemonListModel(PokemonListModels pokemonListModels) {
    _pokemonListModels.pokemonList.addAll(pokemonListModels.pokemonList);
    _appState = HomeState.success;
    setDisplayFilter();
    // notifyListeners();
  }

  Future<void> getData() async {
    var response = await pokemonRepository.getApiPokemon(_offsetApi);
    if (response is Success) {
      setPokemonListModel(response.response as PokemonListModels);
    } else if (response is Failure) {
      setErrorMessage(response.errorResponse.toString());
    }
  }

  /// ==================================================================///

  void setPokemonDetailModels(PokemonDetailModels pokemonDetailModels) {
    _pokemonDetailModels = pokemonDetailModels;
    _appState = HomeState.success;
    notifyListeners();
  }

  void onLoadingDetail(int index) async {
    _appState = HomeState.loading;
    _index = index;
    await getDetail();
  }

  Future<void> getDetail() async {
    final response = await pokemonDetailRepository.getApiDetail(_index);
    if (response is Success) {
      setPokemonDetailModels(response.response as PokemonDetailModels);
    } else if (response is Failure) {
      setErrorMessage(response.errorResponse.toString());
    }
  }

  /// ==================================================================///

  void setFilterText(String filterText){
    _filter = filterText;
    setDisplayFilter();
  }
  void setDisplayFilter() {
    log('filterText: $_filter');
    _pokemonListModelsFilter.pokemonList = [];
    _pokemonListModelsFilter.pokemonList.addAll(_pokemonListModels.pokemonList.where((element) => element.name.contains(_filter)));
    log('Filter Pokemon card: ${_pokemonListModelsFilter.pokemonList.length.toString()}');
    log('All Pokemon card: ${_pokemonListModels.pokemonList.length.toString()}');
    notifyListeners();
  }
}
