const int limitApi = 15;
const String pokemonPath = 'https://pokeapi.co/api/v2/pokemon?limit=$limitApi&offset=';
const String pokemonImagePath =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/';
const String pokemonDetailPath = 'https://pokeapi.co/api/v2/pokemon/';
