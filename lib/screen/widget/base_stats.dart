import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pokemon_app/models/pokemon_detail_model.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class BaseStats extends StatelessWidget {
  final List<Stat> listStat;
  const BaseStats({super.key, required this.listStat});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text('Base Stats',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.green),),
        const SizedBox(height: 10.0),

        // ...listStat.map((e) => _buildState(context, e)).toList(),
        // Expanded(
        //   flex: 4,
        //     child: ListView.builder(
        //         shrinkWrap: true,
        //         itemCount: a.length,
        //         itemBuilder: (context, index) {
        //           return ListTile(
        //             title: Text(a[index].toString()),
        //             trailing: const LinearProgressIndicator(
        //               backgroundColor: Colors.grey,
        //               value: 0.5,
        //               // color: Colors.red,
        //             )
        //           );
        //         }),
        // )
        //

        Row(
          children: [
            Expanded(flex: 1, child: Text('HP : ${listStat[0].baseStat} ')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(flex: 1, child: Text('ATK : ${listStat[1].baseStat} ')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(flex: 1, child: Text('DEF : ${listStat[2].baseStat} ')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(flex: 1, child: Text('SATK : ${listStat[3].baseStat} ')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(flex: 1, child: Text('SDEF : ${listStat[4].baseStat} ')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(flex: 1, child: Text('SPD : ${listStat[5].baseStat}')),
            const Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: StepProgressIndicator(
                  size: 10,
                  totalSteps: 100,
                  currentStep: 50,
                  padding: 0,
                  selectedColor: Colors.green,
                  unselectedColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildState(BuildContext context,Stat stat){
    return Row(
      children: [
        Expanded(
          flex: 1,
            child: Text(
              '${stat.stat.name} : ${stat.baseStat}'
            ),
        )
      ],
    );
  }
}
