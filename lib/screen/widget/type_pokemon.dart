import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon_app/models/pokemon_detail_model.dart';

class TypePokemon extends StatelessWidget {
  final List<Type> slotPokemon;
  const TypePokemon({super.key, required this.slotPokemon});

  // pokemonDetailModel.pokemonDetailModels!.types
  @override
  Widget build(BuildContext context) {
    final List<int> a = [1, 2, 3];
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ...slotPokemon.map((item) => decorateText(item.type.name)).toList(),
      ],
    );
  }

  Widget decorateText(String typeText) {
    return Container(
      width: 60.w,
      height: 20.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.green,
      ),
      child: Center(
        child: Text(
          typeText,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
