import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:pokemon_app/view_models/pokemon_view_models.dart';
import 'package:provider/provider.dart';

class SearchField extends StatefulWidget {
  const SearchField({super.key});

  @override
  State<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  final _formKey = GlobalKey<FormState>();
  final FocusNode _searchFocusNode = FocusNode();
  final TextEditingController _controller = TextEditingController();
  String _userInput = '';
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer(const Duration(seconds: 1), () {});
  }

  void _updateUserInput(String value) {
    if (_timer.isActive) {
      _timer.cancel();
    }
    _userInput = value;

    Future.delayed(const Duration(milliseconds: 500),(){
      log('Delay: $_userInput');
    });
    _timer = Timer(const Duration(milliseconds: 500), () {
      log('Timer: $_userInput');
      context.read<PokemonViewModels>().setFilterText(_userInput);
    });
  }

  @override
  Widget build(BuildContext context) {
    FocusManager.instance.primaryFocus?.unfocus();
    return Form(
      key: _formKey,
      child: TextFormField(
        controller: _controller,
        focusNode: _searchFocusNode,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 10),
          filled: true,
          fillColor: Colors.white,
          prefixIconColor: Colors.blue,
          hintText: 'Search',
          prefixIcon: const Icon(
            Icons.search,
          ),
          suffixIcon: IconButton(
            icon: const Icon(Icons.close),
            onPressed: (){
              _controller.text = '';
              _updateUserInput(_controller.text);
            },
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
        ),
        // validator: (value) {
        //   if (value.isEmpty) {
        //     return 'Please enter your name';
        //   }
        //   return null;
        // },
        onChanged: (value){
          _updateUserInput(value);
        },
      ),
    );
  }
}
