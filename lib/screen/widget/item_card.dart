import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon_app/models/pokemon_list_model.dart';
import 'package:pokemon_app/screen/views/pokemon_detail.dart';
import 'package:pokemon_app/utils/constants.dart';

class ItemCard extends StatelessWidget {
  final Pokemon pokemon;
  final String filterText;
  const ItemCard({super.key, required this.pokemon, required this.filterText});

  /// indexOf
  // List<TextSpan> highlightOccurrences(String source, String query) {
  //   // int startingIndex = 0;
  //   // List<TextSpan> children = [];
  //   // TextStyle highlightedStyle = TextStyle(
  //   //   color: Colors.red, // เปลี่ยนสีไฮไลต์ตามต้องการ
  //   //   fontWeight: FontWeight.bold, // เพิ่มขนาดตัวอักษรไฮไลต์ตามต้องการ
  //   // );
  //   String pokemonName = pokemon.name;

  //   // for(int i=0;i<10;i++){
  //   //   int index = pokemonName.indexOf(filterText,startingIndex);
  //   //   log('$pokemonName: ${pokemonName[index]}: a${filterText}a: ${pokemonName.contains(filterText)} ${index.toString()}');
  //   //   if (index == -1) {
  //   //     children.add(TextSpan(text:pokemonName.substring(startingIndex)));
  //   //     break;
  //   //   } else {
  //   //     children.add(TextSpan(text: pokemonName.substring(startingIndex, index)));
  //   //     children.add(TextSpan(text: filterText, style: highlightedStyle));
  //   //     startingIndex = index + filterText.length;
  //   //   }
  //   // }
  //   // return RichText(
  //   //   text: TextSpan(
  //   //     style: TextStyle(color: Colors.black),
  //   //     children: children,
  //   //   ),
  //   //   textAlign: TextAlign.center,
  //   // );
  // }

  List<TextSpan> highlightOccurrences(String source, String query) {
    if (query.isEmpty || !source.toLowerCase().contains(query.toLowerCase())) {
      return [TextSpan(text: source)];
    }
    final matches = query.toLowerCase().allMatches(source.toLowerCase());

    int lastMatchEnd = 0;

    final List<TextSpan> children = [];
    for (var i = 0; i < matches.length; i++) {
      final match = matches.elementAt(i);

      if (match.start != lastMatchEnd) {
        children.add(TextSpan(
          text: source.substring(lastMatchEnd, match.start),
        ));
      }

      children.add(
        TextSpan(
          text: source.substring(match.start, match.end),
          style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.red,backgroundColor: Colors.yellow),
        ),
      );

      if (i == matches.length - 1 && match.end != source.length) {
        children.add(
          TextSpan(
            text: source.substring(match.end, source.length),
          ),
        );
      }

      lastMatchEnd = match.end;
    }
    return children;
  }

  @override
  Widget build(BuildContext context) {
    final listSplit = pokemon.url.split('/');
    final index = listSplit[listSplit.length - 2];
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PokemonDetail(name: pokemon.name ?? '', index: int.parse(index)),
          ),
        );
      },
      child: Stack(
        children: [
          Container(
            decoration: ShapeDecoration(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              shadows: const [
                BoxShadow(
                  color: Color(0x33000000),
                  blurRadius: 3,
                  offset: Offset(0, 1),
                  spreadRadius: 1,
                )
              ],
            ),
            // BoxDecoration(
            //   color: Colors.white,
            //   borderRadius: BorderRadius.circular(8),
            // ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              // padding: EdgeInsets.only(top: 48),
              height: 44,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color(0xFFEFEFEF),
                borderRadius: BorderRadius.circular(7),
              ),
            ),
          ),

          ///อีกวิธีนึง นอกจาก Align คือใช้ Position
          // Positioned(
          //   bottom: 0,
          //   left: 0,
          //   right: 0,
          //   child: Container(
          //     // padding: EdgeInsets.only(top: 48),
          //     height: 48,
          //     width: double.infinity,
          //     decoration: BoxDecoration(
          //         color: const Color(0xFFEFEFEF),
          //         borderRadius: BorderRadius.circular(7)
          //     ),
          //   ),
          // ),

          ///pokemon name
          Positioned(
            bottom: 4,
            left: 0,
            right: 0,

            // child: highlightText(),
            child: Text.rich(
              TextSpan(
                children: highlightOccurrences(pokemon.name, filterText),
                style: TextStyle(
                  color: const Color(0xFF1C1C1C),
                  fontSize: 10.sp,
                  fontWeight: FontWeight.w400,
                ),
              ),
              textAlign: TextAlign.center,
            ),
          ),

          ///pokemon image
          Align(
            alignment: Alignment.center,
            child: Image.network(
              '$pokemonImagePath$index.png',
              height: 72.h,
              width: 72.w,
            ),
          ),

          ///pokemon number
          Positioned(
            top: 4,
            right: 8,
            child: Text(
              index.padLeft(3, '0'),
              style: TextStyle(
                color: const Color(0xFF666666),
                fontSize: 8.sp,
                fontWeight: FontWeight.w400,
              ),
            ),
          )
        ],
      ),
    );
  }
}
