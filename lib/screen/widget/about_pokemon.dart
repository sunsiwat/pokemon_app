import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AboutPokemon extends StatelessWidget {
  final int weight;
  final int height;
  const AboutPokemon({super.key, required this.weight, required this.height});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          'About',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.green,
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Row(
                  children: [
                    const Icon(Icons.monitor_weight_outlined),
                    Text('$weight kg')
                  ],
                ),
                const Text('Weight'),

              ],
            ),
            Container(
              width: 1.w,
              height: 30.h,
              color: Colors.grey.withOpacity(0.5),
            ),
            Column(
              children: [
                Row(
                  children: [
                    const Icon(Icons.height),
                    Text('$height m')
                  ],
                ),
                const Text('Height')
              ],
            )
          ],
        )
      ],
    );
  }
}
