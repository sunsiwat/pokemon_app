import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon_app/screen/widget/about_pokemon.dart';
import 'package:pokemon_app/screen/widget/base_stats.dart';
import 'package:pokemon_app/screen/widget/type_pokemon.dart';
import 'package:pokemon_app/utils/constants.dart';
import 'package:pokemon_app/view_models/pokemon_view_models.dart';
import 'package:provider/provider.dart';

class PokemonDetail extends StatefulWidget {
  final String name;
  final int index;
  const PokemonDetail({super.key, required this.name, required this.index});

  @override
  State<PokemonDetail> createState() => _PokemonDetailState();
}

class _PokemonDetailState extends State<PokemonDetail> {
  @override
  void initState() {
    context.read<PokemonViewModels>().onLoadingDetail(widget.index);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final pokemonDetailModel = context.watch<PokemonViewModels>();
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.name,
              style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            Text(
              '#${widget.index.toString().padLeft(3, '0')}',
              style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ],
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
        color: Colors.green,
        width: double.infinity,
        height: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(
              child: Image.network(
                '$pokemonImagePath${widget.index}.png',
                fit: BoxFit.cover,
                height: 200.h,
                width: 200.w,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.w),
                margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),

                /// Cannot provide both a color and a decoration
                // color: Colors.white,
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.r)),
                child: _buildState(context, pokemonDetailModel),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildState(BuildContext context, PokemonViewModels pokemonDetailModel) {
    switch (pokemonDetailModel.appState) {
      case HomeState.initial:
      case HomeState.loading:
        return const Center(child: CircularProgressIndicator());
      case HomeState.success:
        return _buildDetail(context, pokemonDetailModel);
      case HomeState.error:
        return Center(
          child: Text(
            pokemonDetailModel.errorMessage,
            style: TextStyle(fontSize: 24.sp, color: Colors.red),
          ),
        );
    }
  }

  Widget _buildDetail(BuildContext context, PokemonViewModels pokemonDetailModel) {
    return Column(
      children: [
        TypePokemon(slotPokemon: pokemonDetailModel.pokemonDetailModels!.types),
        SizedBox(
          height: 10.h,
        ),
        AboutPokemon(
          weight: pokemonDetailModel.pokemonDetailModels!.weight,
          height: pokemonDetailModel.pokemonDetailModels!.height,
        ),
        SizedBox(
          height: 10.h,
        ),
        BaseStats(listStat: pokemonDetailModel.pokemonDetailModels!.stats),
      ],
    );
  }
}
