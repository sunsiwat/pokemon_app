import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon_app/screen/widget/item_card.dart';
import 'package:pokemon_app/screen/widget/search_field.dart';
import 'package:pokemon_app/view_models/pokemon_view_models.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    context.read<PokemonViewModels>().getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final pokemonViewModels = context.watch<PokemonViewModels>();
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Pokemon Demo',
            style: TextStyle(
              fontSize: 30.sp,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        backgroundColor: Colors.red,
      ),
      body: Container(
        color: Colors.red,
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SearchField(),
            SizedBox(
              height: 10.h,
            ),
            _buildItemCard(pokemonViewModels),
          ],
        ),
      ),
    );
  }

  Widget _buildItemCard(PokemonViewModels pokemonViewModel) {
    switch (pokemonViewModel.appState) {
      case HomeState.initial:
      case HomeState.loading:
        return const Center(child: CircularProgressIndicator());
      case HomeState.success:
        return Expanded(
          child: SmartRefresher(
            controller: pokemonViewModel.refreshController,
            enablePullDown: true,
            enablePullUp: true,
            onLoading: () => pokemonViewModel.onLoading(),
            onRefresh: () => pokemonViewModel.onRefresh(),
            header: const WaterDropHeader(),
            child: GridView.builder(
              itemCount: pokemonViewModel.pokemonListModelsFilter.pokemonList.length,
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 24.h),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
              ),
              itemBuilder: (BuildContext context, int index) {
                return ItemCard(
                  pokemon: pokemonViewModel.pokemonListModelsFilter.pokemonList[index],
                  filterText: pokemonViewModel.filter,
                  // index: index+1,
                );
              },
            ),
          ),
        );
      case HomeState.error:
        return Center(
          child: Text(
            pokemonViewModel.errorMessage,
            style: TextStyle(fontSize: 24.sp, color: Colors.red),
          ),
        );
    }
  }
}
